#  groups.sh
#  
#  Copyright 2015 th3d00m <pabloze@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

# Examples
# g_createGroup --group testers
# g_checkGroup --group testers
# g_checkGroup --group testers --member test
# g_deleteGroup --group testers
# g_LocalGroups --newgroup testers --user test --action addgroup
# g_LocalGroups --newgroup testers --user test --action changeprimarygroup
# g_LocalGroups --newgroup testers --user test --action deletefromgroup
# g_LocalGroups --newgroup testers --user test --action deletegroups

#  
#  name: LocalGroups
#  @param Name of NewGroup and username
#  @@print list user details
#  
g_LocalGroups()
{
    # Get options
    for o; do
        case "${o}" in
            --newgroup)      shift; newgroup="${1}"; shift; ;;
            --user)          shift; user="${1}"; shift; ;;
            --action)        shift; action="${1}"; shift; ;;
        esac
    done
    
    # options
    # addgroup
    # changeprimarygroup
    # deletegroups
    # deletefromgroup
    
    if [ -z ${action} ] || [ "${action}" = "addgroup" ];then
        if (usermod -G ${newgroup} ${user} > /dev/null);then
            g_checkGroup --group ${newgroup} --member ${user}
            return 0
        fi
    elif [ "${action}" = "changeprimarygroup" ];then
        if (usermod -g ${newgroup} ${user} > /dev/null);then
            g_checkGroup --group ${newgroup} --member ${user}
            return 0
        fi
    elif [ "${action}" = "deletegroups" ];then
        if (usermod -g "" ${user} > /dev/null);then
            g_checkGroup --group ${newgroup} --member ${user}
            return 0
        fi
    elif [ "${action}" = "deletefromgroup" ];then
        if (delgroup ${user} ${newgroup} > /dev/null);then
            g_checkGroup --group ${newgroup} --member ${user}
            return 0
        fi
    fi
}
#  
#  name: create
#  @param groupName
#  @@print groupName validation
#  
g_createGroup()
{
    # Get options
    for o; do
        case "${o}" in
            --group)          shift; group="${1}"; shift; ;;
        esac
    done
    
    if (addgroup ${group} > /dev/null);then
        return 0
    fi
}
#  
#  name: g_deleteGroup
#  @param groupName
#  @@print groupName validation
#  
g_deleteGroup()
{
    # Get options
    for o; do
        case "${o}" in
            --group)          shift; group="${1}"; shift; ;;
        esac
    done
    
    if (delgroup ${group} > /dev/null);then
        return 0
    fi
}
#  
#  name: g_checkGroup
#  @param groupName
#  @@print groupName validation
#  
g_checkGroup()
{
    # Get options
    for o; do
        case "${o}" in
            --group)        shift; group="${1}"; shift; ;;
            --mode)         shift; mode="${1}"; shift; ;; # im=interactive mode; ni=noninteractive
            --member)       shift; member="${1}"; shift; ;;
        esac
    done
    
    if (grep "${group}:" /etc/group > /dev/null);then
        echo "Group ${group} found, loading info"
        groupGID=$(grep "${group}:" /etc/group|awk -F: '{ print $3 }')
        groupName=$(grep "${group}:" /etc/group|awk -F: '{ print $1 }')
        groupMembers=$(grep "${group}:" /etc/group|awk -F: '{ print $4 }')
        
        if [ ! -z ${group} ] && [ ! -z ${member} ];then
            if echo ${groupMembers} | grep -q "${user}"; then
                echo "${member} is a member of group ${group}"
                return 0
            else
                echo "${member} not is a member of group ${group}"
            fi
        else
            if [ "${mode}" = "im" ];then
                echo "Groupname not exists, This sure your group is ${group}??, Enter your groupname now"
                read group
                g_checkGroup --group ${group} --mode ${mode}
            fi
        fi
    elif [ "${mode}" = "ni" ] || [ -z ${mode} ];then
        echo "Group ${group} not exists"
    else
        echo "None option"
    fi
}
