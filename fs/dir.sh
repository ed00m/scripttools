#  dir.sh
#  
#  Copyright 2015 th3d00m <pabloze@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

# Examples
# checkDir  --path /home/test2 || checkDir  --path /home/test2 --mode im

#  
#  name: checkDir
#  @param PathDir
#  @return 0 if exists
#  
checkDir()
{
    # Get options
    for o; do
        case "${o}" in
            --path)         shift; path="${1}"; shift; ;;
            --user)          shift; user="${1}"; shift; ;;
            --mode)          shift; mode="${1}"; shift; ;; # im=interactive mode; ni=noninteractive
        esac
    done
    
    if [ ! -d ${path} ];then
        if [ "${mode}" = "ni" ] || [ -z ${mode} ];then
            echo "Path ${path} not found"
        elif [ "${mode}" = "im" ];then
            echo "This sure your directory is ${path}??, Enter your path now"
            read path
            checkDir ${path}
        fi
    else
        echo "${path}, Path found"
        return 0
    fi
}
